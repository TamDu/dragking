﻿using UnityEngine;
using System.Collections;

public class DestroyOtherGameObjects : MonoBehaviour 
{
	private void OnCollisionEnter(Collision colObj)
	{
		Destroy(colObj.gameObject);
	}
	
	private void OnTriggerEnter(Collider colObj)
	{
		Destroy(colObj.gameObject);
	}
}
