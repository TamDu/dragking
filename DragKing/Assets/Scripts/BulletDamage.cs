﻿using UnityEngine;
using System.Collections;

public class BulletDamage : MonoBehaviour 
{
	private PlayerHealth playerHp;
	
	private void Start()
	{
		playerHp = GameObject.FindGameObjectWithTag("GameController").GetComponent<PlayerHealth>();
	}

	private void OnCollisionEnter(Collision colObj)
	{
		if(colObj.gameObject.tag == "Player")
		{
			playerHp.modifyPlayerHealth(-10);
			Destroy(gameObject);
		}
	}
	
	private void OnTriggerEnter(Collider colObj)
	{
		if(colObj.gameObject.tag == "Player")
		{
			playerHp.modifyPlayerHealth(-10);
			Destroy(gameObject);
		}
	}
}
