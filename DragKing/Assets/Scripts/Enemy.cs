﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	public GameObject player;
	public float buleltSpeed;
	public Rigidbody bulletPrefab;
	public float fireRate;
	private float currentTime;
	public Transform spawnPos;
	public float randomThreshold;
	public Transform playerWorldPos;
	public float magnitudeComp;
	public Animator anim;

	
	private void Start()
	{
		player = GameObject.Find("Bone020").GetComponentInChildren<ParentRigidBody>().gameObject;;//.GetComponentInChildren<ParentRigidBody>().gameObject;
		playerWorldPos = player.transform;
	}
	
	private void Update()
	{
		rotateToPlayer();
		currentTime -= Time.deltaTime * 1/Time.timeScale;
		if(currentTime <= 0
		   && transform.position.z >= playerWorldPos.position.z
			)
		{
		//	print(currentTime);
		//	print("currentPos" + transform.position.z + "vs" + "Player world" + playerWorldPos.position.z);
			fireAtPlayer();
			currentTime = fireRate;
		}
	}
	
	private void fireAtPlayer()
	{
		Vector3 playerPosition = player.transform.position;
		if (player.GetComponent<Rigidbody>().velocity.magnitude < 7f)
		{
			playerPosition.z += (player.GetComponent<Rigidbody>().velocity.magnitude + player.GetComponent<Rigidbody>().velocity.magnitude/magnitudeComp);
		}
		else if (player.GetComponent<Rigidbody>().velocity.magnitude < 14 && player.GetComponent<Rigidbody>().velocity.magnitude > 7)
		{
			playerPosition.z += player.GetComponent<Rigidbody>().velocity.magnitude + player.GetComponent<Rigidbody>().velocity.magnitude/magnitudeComp;
		}
		else if (player.GetComponent<Rigidbody>().velocity.magnitude > 30)
		{
			playerPosition.z += (player.GetComponent<Rigidbody>().velocity.magnitude/magnitudeComp + player.GetComponent<Rigidbody>().velocity.magnitude/magnitudeComp);
		}
		else
		{
			playerPosition.z += (player.GetComponent<Rigidbody>().velocity.magnitude/magnitudeComp);
		}
		Vector3 dirToPlayer = playerPosition - transform.position;
	//	dirToPlayer.Normalize();
		Vector3 randomDir = new Vector3(dirToPlayer.x + Random.Range(-randomThreshold, (randomThreshold)), dirToPlayer.y + Random.Range(0, 0.1f), dirToPlayer.z + Random.Range(-randomThreshold, randomThreshold)); //Random.Range(-randomThreshold, randomThreshold) = Z
	//	Vector3 randomDir = new Vector3(dirToPlayer.x, dirToPlayer.y, dirToPlayer.z);
	//	print(randomDir);
//		print(player.GetComponent<Rigidbody>().velocity.magnitude);
	//	playAnimation();
		Rigidbody clone = Instantiate(bulletPrefab, spawnPos.position, Quaternion.identity) as Rigidbody;
		if (clone != null && clone.GetComponent<Rigidbody>() != null)
			clone.GetComponent<Rigidbody>().AddForce(randomDir.normalized * buleltSpeed * Time.fixedDeltaTime, ForceMode.Impulse);
	}
	
	private void playAnimation()
	{
	//	anim.SetBool("Fire", true);
		// = 0.1f;
	}
	
	private void rotateToPlayer()
	{
		transform.LookAt(player.transform.position);
	}
	
	private void OnCollisionEnter(Collision colObj)
	{
		Destroy(gameObject);
	}
}
