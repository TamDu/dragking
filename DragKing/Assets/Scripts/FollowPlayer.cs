﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour 
{
	public Vector3 followOffset;
	public Transform player;
	
	private void Update()
	{
		followPlayer();
	}
	
	private void followPlayer()
	{
		Vector3 followPos = new Vector3(0f, 0f, player.position.z);
		transform.position = followPos + followOffset;
	}
}
