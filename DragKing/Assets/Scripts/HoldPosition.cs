﻿using UnityEngine;
using System.Collections;

public class HoldPosition : MonoBehaviour 
{
	public Transform movePoint;
	
	void Update () 
	{
		transform.position = movePoint.transform.position;
	}
}
