﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour 
{
	public bool canPauseMenu;
	public GameObject[] pauseButtons;

	private void Start()
	{
		Time.timeScale = 1;
	}

	private void Update()
	{
		if (canPauseMenu)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				bool isActive = false;
				for (int i = 0; i < pauseButtons.Length; i++)
				{
					isActive = pauseButtons[i].activeSelf;
					pauseButtons[i].SetActive(!isActive);
				}
				if (isActive)
				{
					Time.timeScale = 1;
				}
				else
				{
					Time.timeScale = 0;
				}
			}
		}
	}

	public void changeLevel(int level)
	{
		PlayerPrefs.DeleteAll();
		Application.LoadLevel(level);
	}
	
	public void restartLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
	
	public void changeQuality(int qualityLevel)
	{
		Time.timeScale = 1;
		QualitySettings.SetQualityLevel(qualityLevel);
	}
}
