﻿using UnityEngine;
using System.Collections;

public class MoveJoints : MonoBehaviour 
{
	public GameObject planePrefab;
	private Ray camRay;
	private Rigidbody hitRgb;
	public LayerMask movePart;
	public LayerMask jointHitBox;
	public float jointSpeed;
	private GameObject movementPlane;
	public float radius;
	public Transform player;
	private Vector3 movePlaneSpawnPos;
	
	private void Update()
	{
		checkMouseClicks();
	}

	private void checkMouseClicks()
	{
		if (Input.GetMouseButtonDown(0)
		    //    && Time.timeScale <= 0
		    )
		{
			camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit rayHit;
			if (Physics.Raycast(camRay, out rayHit, 50f))
			{
				if (rayHit.transform.GetComponent<Rigidbody>() != null)
				{
					hitRgb = rayHit.transform.GetComponent<Rigidbody>();
//					print(hitRgb.gameObject.name);
					ParentRigidBody parentRgbComponent = hitRgb.GetComponent<ParentRigidBody>();
					if (parentRgbComponent != null)
					{
						movementPlane = Instantiate(planePrefab, player.transform.position, Quaternion.identity) as GameObject;
						movePlaneSpawnPos = player.transform.position;
					}
					else
					{
						hitRgb = null;
					}
				}
			}
		}
		
		if (Input.GetMouseButton(0)
		    //   && Time.timeScale <= 0
		    )
		{
			camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit rayHit;
			if (Physics.Raycast(camRay, out rayHit, 50f, movePart) && hitRgb != null)
			{
				float distance = Vector3.Distance(rayHit.point, hitRgb.transform.position);
				if (distance > 0.1f)
				{
					//print("Is working");
					//	Vector3 dir = rayHit.point - hitRgb.transform.position;
					//dir = dir.normalized * jointSpeed * Time.unscaledDeltaTime;
					//	print(dir);
					//	hitRgb.MovePosition(hitRgb.transform.position + dir);
				/*	if (Time.timeSinceLevelLoad > 10)
					{
				//		movementPlane.transform.position = new Vector3(movementPlane.transform.position.x, movementPlane.transform.position.y, player.transform.position.z);
					}
				//	else
					{
						if (player.transform.position.z < movePlaneSpawnPos.z)
						{
							movementPlane.transform.position = new Vector3(movementPlane.transform.position.x, movementPlane.transform.position.y, movePlaneSpawnPos.z);
						}
						else*/
						//	print(hitRgb.gameObject.name);
						if (hitRgb.gameObject.name != "Bone019")
						{
							//movementPlane.transform.position = new Vector3(movementPlane.transform.position.x, movementPlane.transform.position.y, movePlaneSpawnPos.z);
							movementPlane.transform.position = new Vector3(movementPlane.transform.position.x, movementPlane.transform.position.y, player.transform.position.z);
							changePosition(rayHit.point);
						}
						else
						{
						//	Time.timeScale = 0;
						}
					//}
					//print(hitRgb.position.z);
				}
			}
		}
		else if (movementPlane != null)
		{
			hitRgb = null;
			Destroy(movementPlane);
		}
	}
			
	private void changePosition(Vector3 rayHitPos)
	{
		float distance = Vector3.Distance(rayHitPos, hitRgb.GetComponent<ParentRigidBody>().parent.position);
		if (distance >= radius)
		{
		//	print(radius);
			Vector3 dirToHitPoint = rayHitPos - hitRgb.GetComponent<ParentRigidBody>().parent.position;
			dirToHitPoint.Normalize();
			dirToHitPoint = dirToHitPoint * radius + hitRgb.GetComponent<ParentRigidBody>().parent.position;
			hitRgb.transform.position = dirToHitPoint;
		}
		else
		{
			hitRgb.transform.position = rayHitPos;
		}
	}
}
