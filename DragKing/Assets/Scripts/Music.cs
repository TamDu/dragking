﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour 
{
	private AudioSource gameAudio;
	public AudioClip levelMusic;
	public AudioClip endLevel;
	
	private void Start()
	{
		gameAudio = GetComponent<AudioSource>();
	}
	
	private void Update()
	{
		changeMusic();
	}
	
	private void changeMusic()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			gameAudio.Play();
			gameAudio.clip = endLevel;
			//gameAudio.PlayOneShot(levelMusic);
		}
	}
}
