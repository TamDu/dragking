﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour 
{
	public float playerHealth;
	public Slider playerHealthBar;
	public Text rekted;
	public Button[] loseScreenButtons;
	public AudioSource audio;
	public AudioClip clip;
	
	public void modifyPlayerHealth(float modifier)
	{
		playerHealth += modifier;
		playerHealthBar.value = playerHealth;
		checkHealth();
		//print(Time.timeScale);
	}
	
	private void checkHealth()
	{
		if (playerHealth <= 0)
		{	
			audio.Stop();
			audio.PlayOneShot(clip);
			Time.timeScale = 0;
			//loseTxt.gameObject.SetActive(true);
			for (int i = 0; i < loseScreenButtons.Length; i++)
			{
				loseScreenButtons[i].gameObject.SetActive(true);
			}
			rekted.gameObject.SetActive(true);
		}
	}
}
