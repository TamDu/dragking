﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct KeyBinds
{
	public KeyCode up;
	public KeyCode down;
	public KeyCode left;
	public KeyCode right;
}

public class PlayerMovement : MonoBehaviour 
{
	public KeyBinds keys;
	public float speed;
	public float maxSpeed;
	private Rigidbody myRgb;
	private Vector3 playerStartPosition;
	public float forwardSpeed;
	public float maxForwardSpeed;
	public float time;
	//private Vector3 playerMovement;
	
	private void Start()
	{
		myRgb = GetComponent<Rigidbody>();
		playerStartPosition = transform.position;
	}
	
	private void Update()
	{
		time -= Time.deltaTime;
	}
	
	private void FixedUpdate()
	{
	//	movePlayer(Vector3.up, speed, keys.up);
		//movePlayer(Vector3.back, speed, keys.down);
	//	movePlayer(Vector3.right, speed, keys.right);
	//	movePlayer(Vector3.left, speed, keys.left);
	//	movePlayer(Vector3.up, speed, keys.up);
	//	movePlayer(Vector3.down, speed, keys.down);
		if (time > 0)
		{
			myRgb.AddForce(Vector3.forward * forwardSpeed * 5);
		}
		else
		{
			myRgb.AddForce(Vector3.forward * forwardSpeed);
		}
		capSpeed();
		
	//	myRgb.MovePosition(transform.position + Vector3.forward);
		
		myRgb.AddForce(Vector3.down * speed * Time.fixedDeltaTime * 1/Time.timeScale);
		placeMovementRestrictions();
	}
	
	private void placeMovementRestrictions()
	{
		if (transform.position.y < playerStartPosition.y)
		{
			transform.position = new Vector3(transform.position.x, playerStartPosition.y,transform.position.z);
		}
		else if (transform.position.y > 1)
		{
			transform.position = new Vector3(transform.position.x, 1f,transform.position.z);
		}
		
		if (transform.position.x > 2)
		{
			transform.position = new Vector3(2, transform.position.y, transform.position.z);
		}
		else if (transform.position.x < -2)
		{
			transform.position = new Vector3(-2, transform.position.y, transform.position.z);
		}
	}
	
	private void movePlayer(Vector3 direction, float speed, KeyCode movementKey)
	{
		if (Input.GetKey(movementKey))
		{
			myRgb.AddForce(direction * speed * Time.fixedDeltaTime * 1/Time.timeScale);
			//playerMovement = direction.normalized * speed * Time.deltaTime;
			//myRgb.MovePosition(transform.position + playerMovement);
		}
	}
	
	private void capSpeed()
	{
		if (myRgb.velocity.magnitude >= maxSpeed)
		{
			myRgb.velocity = myRgb.velocity.normalized * maxSpeed * Time.fixedDeltaTime * 1/Time.timeScale;
		}
	
		if (myRgb.velocity.magnitude >= maxForwardSpeed)
		{
			myRgb.velocity = myRgb.velocity.normalized * maxForwardSpeed * Time.fixedDeltaTime * 1/Time.timeScale;
		}
	}
}
