﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour 
{
	public float rotationSpeedX;
	public float rotationSpeedY;
	public float rotationSpeedZ;
	
	private bool worldRotate;
	
	private void Start()
	{
		//Time.timeScale = 0f;
	//	Time.unscaledTime
	}
	
	void Update () 
	{
		if (Input.GetKey(KeyCode.LeftControl))
		{
			worldRotate = !worldRotate;
		}
		
		if (worldRotate)
		{
			transform.Rotate(rotationSpeedX, rotationSpeedX, rotationSpeedZ, Space.World);
		}
		else
		{
			transform.Rotate(rotationSpeedX, rotationSpeedX, rotationSpeedZ, Space.Self);
		}
	}
}
