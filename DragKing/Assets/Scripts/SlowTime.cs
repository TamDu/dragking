﻿using UnityEngine;
using System.Collections;

public class SlowTime : MonoBehaviour {

	private void Start()
	{
	}
	
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (Time.timeScale == 1f)
			{
				Time.timeScale = 0.7f;
			}
			else
			{
				Time.timeScale = 1f;
			}
		}
	}
}
