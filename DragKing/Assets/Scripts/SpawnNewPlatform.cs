﻿using UnityEngine;
using System.Collections;

public class SpawnNewPlatform : MonoBehaviour 
{
	public Transform[] platformPrefab;
	public Transform[] enemySpawn;
	public GameObject enemyPrefab;
	public Vector3 spawnPosition;
	private bool hasEnter;
	public GameObject sea;
	
	private void OnTriggerEnter(Collider colObj)
	{
		if (colObj.gameObject.GetComponent<FollowPlayer>() != null
			&& hasEnter == false
			)
		{
			int randomNumber = Random.Range(0, 30);
			if (randomNumber < 10)
			{
				Instantiate(platformPrefab[1], transform.position + spawnPosition, Quaternion.identity);
			}
			else if (randomNumber > 10 && randomNumber < 20)
			{
				Instantiate(platformPrefab[2], transform.position + spawnPosition, Quaternion.identity);
			}
			else
			{
				Instantiate(platformPrefab[0], transform.position + spawnPosition, Quaternion.identity);
			}
			//print(hasEnter);
			randomNumber = Random.Range(4, enemySpawn.Length);
			for (int i = 0; i < randomNumber; i++)
			{ 
				Instantiate(enemyPrefab, enemySpawn[i].position, Quaternion.identity);	
			}
			hasEnter = true;
		}
	}
}
