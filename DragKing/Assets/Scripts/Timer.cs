﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour 
{
	public Text timerUI;
	public float timeLimit;
	public float timeLeft;
	public Text winUI;
	public Button[] menuButtons;
	private bool canTurnTimeOff = true;
	
	private void Start()
	{
		timeLeft = timeLimit;
	}
	
	private void Update()
	{
		displayTimer();
	}
	
	private void displayTimer()
	{
		if (timeLeft > 0)
		{
			timeLeft = timeLimit - Time.timeSinceLevelLoad;
			float minutes = Mathf.Floor( timeLeft / 60 );
			int seconds = Mathf.RoundToInt( timeLeft % 60 );
			if ( seconds == 60 )
			{
				seconds = 00;
			}
			else
			{
				displayTimer(minutes, seconds);
			}
		}
		else
		{
			if (canTurnTimeOff)
			{
				Time.timeScale = 0;
				canTurnTimeOff = false;
			}
			displayTimer(00, 00);
			winUI.gameObject.SetActive(true);
			for (int i = 0; i < menuButtons.Length; i++)
			{
				menuButtons[i].gameObject.SetActive(true);
			}
		}
	}
	
	private void displayTimer(float minutes, int seconds)
	{
		timerUI.text = string.Format 
			("{00:00} {01:00}",
			 minutes, seconds
			 );
	}
}
