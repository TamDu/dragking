﻿using UnityEngine;
using System.Collections;

public class UIFunctions : MonoBehaviour 
{
	public void loadFirstLevel()
	{
		Time.timeScale = 1f;
		Application.LoadLevel(1);
	}
	
	public void closeGame()
	{
		Application.Quit();
	}
	
	public void loadMainMenu()
	{
		Time.timeScale = 1;
		Application.LoadLevel(0);
	}
}
